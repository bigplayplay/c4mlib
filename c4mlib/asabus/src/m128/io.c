#include "util.h"

/* public functions declaration section ------------------------------------- */
void ASABUS_ID_init(void);
void ASABUS_ID_set(char id);

/* define section ----------------------------------------------------------- */

/* static variables section ------------------------------------------------- */

/* static functions declaration section ------------------------------------- */

/* static function contents section ----------------------------------------- */

/* public function contents section ----------------------------------------- */
void ASABUS_ID_set(char id) {
    if (id > 0x07) {
        return;
    }
    else {
        REGFPT(ID_PORT, ID_MASK, ID_SHIFT, id);
    }
};

void ASABUS_ID_init(void) {
    REGFPT(ID_DDR, ID_MASK, ID_SHIFT, 7);
};

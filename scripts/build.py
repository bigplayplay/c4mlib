import os
import glob
import time
import shutil
import zipfile
import configparser
import ast

from settings import SETTINGS
from genheader import genC4mlibHeaders
from genarchive import genC4mlibArchive

# C4MLIB 的設定標頭檔
LIBCFGS = os.listdir(SETTINGS['library_path']+'config')

def zipFiles(zip_filename):
    shutil.copy(SETTINGS['library_path']+'libc4m.a', 'dist/libc4m.a')

    for cfg in LIBCFGS:
        shutil.copy(SETTINGS['library_path'] + 'config/' + cfg, 'dist/' + cfg)

    if os.path.isfile(zip_filename):
        os.remove(zip_filename)

    cwd = os.getcwd()
    os.chdir('dist')
    with zipfile.ZipFile(zip_filename, 'w') as z:
        z.write('c4mlib.h')
        z.write('libc4m.a')
        for cfg in LIBCFGS:
            z.write(cfg)

    os.chdir(cwd)


def run():
    os.makedirs('dist', exist_ok=True)
    for device in SETTINGS['devices']:
        for compiler in SETTINGS['avr_compilers']:
            print('Start build {} version!'.format(device['name']))
            print('  mcu : {}'.format(device['mcu']))
            print('  fcpu: {}'.format(device['fcpu']))
            genC4mlibArchive(SETTINGS, device, compiler)
            genC4mlibHeaders(SETTINGS, device, 'dist/c4mlib.h')
            filename = 'c4mlib_{}_{}_{}.zip'.format(
                SETTINGS['version'],
                device['name'],
                compiler['name']
            )
            zipFiles(filename)
            print('Complete build ' + device['name'] + ' !\n')

    os.remove('dist/libc4m.a')
    os.remove('dist/c4mlib.h')
    for cfg in LIBCFGS:
        os.remove('dist/'+cfg)


if __name__ == '__main__':
    run()

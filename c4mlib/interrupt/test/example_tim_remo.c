#define F_CPU 11059200UL
#include "c4mlib/asabus/src/remo_reg.h"
#include "c4mlib/asauart/src/asauart_slave.h"
#include "c4mlib/device/src/device.h"
#include "c4mlib/interrupt/src/extint.h"
#include "c4mlib/interrupt/src/intfreqdiv.h"
#include "c4mlib/interrupt/src/timint.h"

#include <avr/interrupt.h>
#include <avr/io.h>
#include <stddef.h>
#include <stdint.h>

#include "c4mlib/config/remo_reg.cfg"

void init_timer0();

void USER_FUNCTION(void* pvData);

/* 主程式區塊 */
int main() {
/* 常數巨集定義區段 */
#define REMOREG_0_SIZE (4)
#define REMOREG_0_INI (0)

    /* Variables 變數宣告及定義區段 */
    // 宣告計時器硬體管理用資料結構
    TypeOfTimInt TimInt0 = TIMINT_0_STR_INI;
    // 宣告串列通訊埠管理用資料結構
    TypeOfSerialIsr SerialIsrUart = SERIAL_ISR_STR_UART_INI;
    // 宣告儲存可遠端讀寫暫存器的地址編號變數
    uint8_t remoreg0AddressId;
    // 宣告儲存計時中斷中執行工作的工作編號變數
    uint8_t TIMFB0Id;
    // 宣告可遠端讀寫變數
    uint32_t remoreg0 = REMOREG_0_INI;

    /* 函式庫內建及使用者自建初始設定函式區段 */

    // 呼叫ASA_DEVICE_set()執行ASA函式庫之初始化設定。
    ASA_DEVICE_set();
    // 執行計時器初始化設定
    printf("Start timer0 with CTC 1000Hz\n");
    init_timer0();
    // 執行計時器硬體管理用資料結構初始化設定
    TimInt_init(&TimInt0);
    // 執行串列通訊埠管理用資料結構初始化設定
    // FIXME: 使用新版初始化
    SerialIsr_init(&SerialIsrUart, 0);
    // 呼叫TimInt_reg()登陸計時中斷中執行工作並取得編號
    TIMFB0Id = TimInt_reg(&TimInt0, USER_FUNCTION, &remoreg0);
    // 呼叫RemoRW_reg()登陸可遠端讀寫變數並取得編號
    remoreg0AddressId = RemoRW_reg(&SerialIsrUart, remoreg0, REMOREG_0_SIZE);

    //回傳可遠端讀寫變數登陸結果(發布階段需刪除)
    printf("Create RemoRWreg [%u] with %u bytes \n", remoreg0AddressId,
           SerialIsrUart.remo_reg[remoreg0AddressId].sz_reg);

    // 呼叫sei()開啟全域中斷
    TimInt_en(&TimInt0, TIMFB0Id, 1);
    sei();

    while (1)
        ;  // Block here
}

/* 副函式區段 */
// 計時中斷中執行工作副函式區段
void USER_FUNCTION(void* pvData) {
    // 執行使用者定義之指定工作
    uint32_t RESULT = 0;
    // 將結果放入先前宣告之可遠端讀寫變數
    uint32_t* p_remoreg0 = (uint32_t*)pvData;
    p_remoreg0 = RESULT;
    return;
}

// Initialize the TIME0 with CTC mode, interrupt at 1000 Hz
void init_timer0() {
    // Pre-scale 128
    // TCCR0 [FOC0 WGM00 COM01 COM00 WGM01 CS02 CS01 CS00]
    // Mode  WGMn1 WGMn0 Mode TOP   TOVn_FlagSet_on
    // 2     1     0     CTC  OCRn  Immediate MAX
    OCR0 = 86 * 2;
    TCCR0 = 0b00001101;
    TCNT0 = 0;
    TIMSK |= 1 << OCIE0;
}

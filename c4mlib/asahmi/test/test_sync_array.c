/**
 * @file test_sync_array.c
 * @author LiYu87
 * @date 2019.07.02
 * @brief
 */

#include "c4mlib/asahmi/src/asa_hmi.h"
#include "c4mlib/device/src/device.h"

int main() {
    ASA_STDIO_init();

    float data[5] = {1.1, -1, 0, 1, -2.1};
    char num = 5;

    HMI_snput_array(HMI_TYPE_F32, num, data);

    HMI_snget_array(HMI_TYPE_F32, num, data);

    HMI_snput_array(HMI_TYPE_F32, num, data);

    return 0;
}

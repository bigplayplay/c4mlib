#define F_CPU 11059200UL
#include "c4mlib/device/src/device.h"
#include "c4mlib/interrupt/src/extint.h"
#include "c4mlib/interrupt/src/intfreqdiv.h"
#include "c4mlib/interrupt/src/timint.h"

#include <avr/interrupt.h>
#include <avr/io.h>
#include <stddef.h>
#include <stdint.h>

// TODO: Initial macro Change to another file
#include "c4mlib/config/interrupt.cfg"
extern TypeOfTimInt* RedirectTableOfTimIntStr[];
TypeOfIntFreqDiv IntFreqDivStr_1;
// TypeOfIntFreqDiv IntFreqDivStr_2;

TypeOfTimInt TimInt3 = TIMINT_3_STR_INI;

// TypeOfExtInt ExtInt0 = EXTINT_0_STR_INI;

void init_timer();
void USER_FUNCTION(void* pvData);

int main() {
    ASA_DEVICE_set();

    /***** Test the  IntFreqDiv component *****/
    printf("======= Test IntFreqDiv component =======\n");

    printf("Setup PORTA[0:7] output\n");
    DDRA = 0xFF;

    printf("Start timer3 with CTC 1000Hz\n");
    init_timer();
    printf("enable Global Interrupt\n");

    uint8_t param_0 = 0;
    uint8_t param_20 = 20;
    uint8_t param_40 = 40;
    uint8_t param_60 = 60;
    uint8_t param_80 = 80;

    //             ISRStr_p          ISRFunc_p      ISRFuncStr_p cycle phase
    IntFreqDiv_reg(&IntFreqDivStr_1, USER_FUNCTION, &param_0, 100, 0);
    printf("Added 100 cycle,0 phase to IntFreqDiv 1 Component\n");
    IntFreqDiv_reg(&IntFreqDivStr_1, USER_FUNCTION, &param_20, 100, 20);
    printf("Added 100 cycle,20 phase to IntFreqDiv 1 Component\n");
    IntFreqDiv_reg(&IntFreqDivStr_1, USER_FUNCTION, &param_40, 100, 40);
    printf("Added 100 cycle,40 phase to IntFreqDiv 1 Component\n");
    IntFreqDiv_reg(&IntFreqDivStr_1, USER_FUNCTION, &param_60, 100, 60);
    printf("Added 100 cycle,60 phase to IntFreqDiv 1 Component\n");
    IntFreqDiv_reg(&IntFreqDivStr_1, USER_FUNCTION, &param_80, 100, 80);
    IntFreqDiv_en(&IntFreqDivStr_1, 0, 1);
    IntFreqDiv_en(&IntFreqDivStr_1, 1, 1);
    IntFreqDiv_en(&IntFreqDivStr_1, 2, 1);
    IntFreqDiv_en(&IntFreqDivStr_1, 3, 1);
    printf("Added 100 cycle,70 phase to IntFreqDiv 1 Component\n");

    printf("Initize the TimInt component with TimInt3\n");
    // Connect to Timer
    TimInt_init(&TimInt3);

    printf("Added IntFreqDiv 1 to TimInt3\n");
    // TimInt_reg(TypeOfTimInt *ISRStr_p, Func_t ISRFunc_p, void *ISRFuncStr_p)
    uint8_t id = TimInt_reg(&TimInt3, IntFreqDiv_step, &IntFreqDivStr_1);
    TimInt_en(&TimInt3, id, 1);
    printf("id is %d\n", id);
    printf("RedirectTableOfTimIntStr %d %d %d\n",
           RedirectTableOfTimIntStr[3]->total,
           RedirectTableOfTimIntStr[3]->task[0].enable,
           RedirectTableOfTimIntStr[3]->task[0].func_p);
    printf("IntFreqDivStr_1- %d %d %d\n", RedirectTableOfTimIntStr[3]->total,
           RedirectTableOfTimIntStr[3]->task[0].enable,
           RedirectTableOfTimIntStr[3]->task[0].func_p);
    sei();

    while (1) {
        ;  // Block here
    }
}

// Initialize the TIME3 with CTC mode, interrupt at 1000 Hz
void init_timer() {
    // Pre-scale 1
    // COMA COMB non-inverting mode
    // 1    0
    // CTC with TOP ICRn
    // WGM3 WGM2 WGM1 WGM0
    // 1    1    0    0
    // TCCR3A  [COM3A1 COM3A0 COM3B1 COM3B0 COM3C1 COM3C0 WGM31 WGM30]
    // TCCR3B  [ICNC3 ICES3 �V WGM33 WGM32 CS32 CS31 CS30]

    ICR3 = 11058 * 2;
    TCCR3A = 0b00000000;
    TCCR3B = 0b00011001;
    TCNT3 = 0x00;
    ETIMSK |= 1 << OCIE3A;
}

void USER_FUNCTION(void* pvData) {
    uint8_t* p_param = (uint8_t*)pvData;

    if (p_param != NULL) {
        switch (*p_param) {
            case 0:
                printf("Y");
                PORTA = 0;
                break;
            case 20:
                printf("Y20");
                PORTA |= 1;
                break;
            case 40:
                printf("Y40");
                PORTA |= 2;
                break;
            case 60:
                printf("Y60");
                PORTA |= 4;
                break;
            case 80:
                printf("Y80");
                PORTA |= 8;
                break;
        }
    }
}

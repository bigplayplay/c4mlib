/**
 * @file extint.h
 * @author Ye cheng-Wei
 * @author LiYu87
 * @date 2019.04.01
 * @brief
 * 提供ASA函式庫標準外部中斷介面，將原生硬體Interrupt呼叫函式占用，並提供登陸函式介面。
 */

// TODO: 完成此份文件註解

#ifndef C4MLIB_COMMON_ASA_EXTINT_H
#define C4MLIB_COMMON_ASA_EXTINT_H

#include "c4mlib/interrupt/src/isr_func.h"
#include "c4mlib/macro/src/std_type.h"

#include <stdint.h>

// Include Configure file
#include "c4mlib/config/interrupt.cfg"

/* Public Section Start */
typedef struct {
    const uint8_t
        EXTI_UID;  // 外部中斷EXTI的編號，應於定義資料結構的同時初始化其編號，之後呼叫_init()函式時完成連線
    uint8_t total;  // 外部中斷已註冊總數
    volatile TypeOfISRFunc
        task[MAX_EXTINT_FuncNum];  // 紀錄所有已註冊的外部中斷
} TypeOfExtInt;

// Initialize the TypeOfExtInt struct
void ExtInt_init(TypeOfExtInt* extInt_p);

// Register EXT Isr instance
uint8_t ExtInt_reg(TypeOfExtInt* extInt_p, Func_t isrFunc_p,
                   void* isrFuncPara_p);

// Control EXT Isr
void ExtInt_en(TypeOfExtInt* extInt_p, uint8_t isr_id, uint8_t enable);
/* Public Section End */

// Called by internal ISR, private function
void ExtInt_step(TypeOfExtInt* extInt_p);

/* Library memory mapping table Start */
TypeOfExtInt* RedirectTableOfExtIntStr[MAX_ISR_EXT_NUM];
/* Library memory mapping table End */

#endif  // C4MLIB_COMMON_ASA_EXTINT_H

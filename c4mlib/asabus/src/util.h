#ifndef C4MLIB_ASABUS_UTIL_H
#define C4MLIB_ASABUS_UTIL_H

#include "c4mlib/macro/src/bits_op.h"
#include "c4mlib/macro/src/std_res.h"

#include <avr/io.h>

#include "pin_def.h"

#endif  // C4MLIB_ASABUS_UTIL_H

#include "c4mlib/hardware/src/acomp.h"

#include "c4mlib/macro/src/bits_op.h"
#include "c4mlib/macro/src/std_res.h"

#include <avr/io.h>

// TODO: HARDWARE ACOMP 尋找負責人，或安排時程

/* public functions declaration section ------------------------------------- */
char ACOMP_fpt(char LSByte, char Mask, char Shift, char Data);
char ACOMP_fgt(char LSByte, char Mask, char Shift, void* Data_p);
char ACOMP_put(char LSbyte, char Bytes, void* Data_p);
char ACOMP_get(char LSbyte, char Bytes, void* Data_p);

char ACOMP_set(char LSByte, char Mask, char Shift, char Data)
    __attribute__((alias("ACOMP_fpt")));

/* define section ----------------------------------------------------------- */

/* static variables section ------------------------------------------------- */

/* static functions declaration section ------------------------------------- */

/* static function contents section ----------------------------------------- */

/* public function contents section ----------------------------------------- */
char ACOMP_fpt(char LSByte, char Mask, char Shift, char Data) {
    return RES_OK;
}

char ACOMP_fgt(char LSByte, char Mask, char Shift, void* Data_p) {
    return RES_OK;
}

char ACOMP_put(char LSbyte, char Bytes, void* Data_p) {
    return RES_OK;
}

char ACOMP_get(char LSbyte, char Bytes, void* Data_p) {
    return RES_OK;
}

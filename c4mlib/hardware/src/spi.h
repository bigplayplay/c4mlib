#ifndef C4MLIB_HARDWARE_SPI_H
#define C4MLIB_HARDWARE_SPI_H

/* Public Section Start */
char SPI_set(char LSByte, char Mask, char Shift, char Data);
char SPI_fpt(char LSByte, char Mask, char Shift, char Data);
char SPI_fgt(char LSByte, char Mask, char Shift, void *Data_p);
char SPI_put(char LSbyte, char Bytes, void *Data_p);
char SPI_get(char LSByte, char Bytes, void *Data_p);
/* Public Section End */

#endif  // C4MLIB_HARDWARE_SPI_H

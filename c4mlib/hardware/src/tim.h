#ifndef C4MLIB_HARDWARE_TIM_H
#define C4MLIB_HARDWARE_TIM_H

/* Public Section Start */
char TIM_set(char LSByte, char Mask, char Shift, char Data);
char TIM_fpt(char LSByte, char Mask, char Shift, char Data);
char TIM_fgt(char LSByte, char Mask, char Shift, void *Data_p);
char TIM_put(char LSbyte, char Bytes, void *Data_p);
char TIM_get(char LSByte, char Bytes, void *Data_p);
/* Public Section End */

#endif  // C4MLIB_HARDWARE_TIM_H

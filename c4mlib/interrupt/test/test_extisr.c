#define F_CPU 11059200UL
#include "c4mlib/device/src/device.h"
#include "c4mlib/interrupt/src/extint.h"

#include <avr/interrupt.h>
#include <avr/io.h>
#include <stddef.h>
#include <stdint.h>

TypeOfExtInt ExtIsr4 = EXTINT_4_STR_INI;
TypeOfExtInt ExtIsr5 = EXTINT_5_STR_INI;
TypeOfExtInt ExtIsr6 = EXTINT_6_STR_INI;
TypeOfExtInt ExtIsr7 = EXTINT_7_STR_INI;

void init_exti();

void USER_FUNCTION(void* pvData);

int main() {
    ASA_STDIO_init();
    ASABUS_ID_init();

    /***** Test the  ExtInt component *****/
    printf("======= Test ExtInt component =======\n");

    printf("Setup PORTA[0:7] output\n");
    DDRA = 0xFF;

    // Initialize external interrupt
    printf("Setup external Interrupt INT4-INT7\n");
    init_exti();

    // Initialize the connect TypeOfTimInt with Internal TIM_isr();
    ExtInt_init(&ExtIsr4);
    ExtInt_init(&ExtIsr5);
    ExtInt_init(&ExtIsr6);
    ExtInt_init(&ExtIsr7);

    uint8_t fg_interrupt0 = 0;
    uint8_t fg_interrupt1 = 1;
    uint8_t fg_interrupt2 = 2;
    uint8_t fg_interrupt3 = 3;

    //          ISRStr_p  ISRFunc_p      ISRFuncParam_p
    ExtInt_reg(&ExtIsr4, USER_FUNCTION, fg_interrupt0);
    printf("Added USER_FUNCTION with fg_interrupt0  to ExtInt4 Component\n");
    ExtInt_reg(&ExtIsr5, USER_FUNCTION, fg_interrupt1);
    printf("Added USER_FUNCTION with fg_interrupt1  to ExtInt5 Component\n");
    ExtInt_reg(&ExtIsr6, USER_FUNCTION, fg_interrupt2);
    printf("Added USER_FUNCTION with fg_interrupt2  to ExtInt6 Component\n");
    ExtInt_reg(&ExtIsr7, USER_FUNCTION, fg_interrupt3);
    printf("Added USER_FUNCTION with fg_interrupt3  to ExtInt7 Component\n");

    printf("Enable Global Interrupt\n");
    sei();

    while (1)
        ;  // Block here
}

// Initialize ext0~ext7
void init_exti() {
    /***** Pin mapping *****/
    // PD0  PD1  PD2  PD3
    // INT0 INT1 INT2 INT3
    // SCL  SDA  RXD1 TXD1
    /***********************/

    /***** Pin mapping *****/
    // PE4  PE5  PE6  PE7
    // INT4 INT5 INT6 INT7
    // OC3B OC3C T3   ICP3
    /***********************/
    // Setup INT7:4 as input pin
    REGFGT(DDRE, 0xF0, 0, 0x00);

    // Setup INT7:4 using rising edge
    EICRB |= 0xFF;

    // Enable INT7:0
    EIMSK = 0xF0;
}

void USER_FUNCTION(void* pvData) {
    uint8_t* fg_interrupt = (uint8_t*)pvData;

    if (fg_interrupt != NULL) {
        switch (*fg_interrupt) {
            case 0:
                PORTA ^= 1;
                break;
            case 1:
                PORTA ^= 2;
                break;
            case 2:
                PORTA ^= 4;
                break;
            case 3:
                PORTA ^= 8;
                break;
        }
    }
}
